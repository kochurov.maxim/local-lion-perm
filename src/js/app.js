(function () {
  'use strict'
  let buttonFilter = document.querySelector('.js-filter');
  let selectAll = document.querySelectorAll('.select');
  let collapseAll = document.getElementsByClassName('js-collapse');

  buttonFilter.addEventListener('click', () => {
    for (let i = 0; i < collapseAll.length; i++) {
      let el = collapseAll[i];
      el.removeEventListener('click', showContentCollapse)
    }
    collapseAll = document.getElementsByClassName('js-collapse');
    addEventCollapse(collapseAll);
  })

  let showContentCollapse = () => {
    let parentCollapse = event.target.closest('.collapse');

    if (parentCollapse.classList.contains('collapse--open')) {
      parentCollapse.classList.remove('collapse--open')
    } else {
      parentCollapse.classList.add('collapse--open')
    }
  }

  let addEventCollapse = (collapseAll) => {
    for (let i = 0; i < collapseAll.length; i++) {
      let el = collapseAll[i];
      el.addEventListener('click', showContentCollapse)
    }
  }
  addEventCollapse(collapseAll);


  // select
  let selectEvent = (select) => {
    if (event.type === 'click') {
      if (select.classList.contains('select--change')) {
        select.classList.remove('select--change')
      } else {
        select.classList.add('select--change')
      }
    }
    if (event.type === 'blur') {
      select.classList.remove('select--change')
    }
  }

  selectAll.forEach((el) => {
    el.addEventListener('click', () => selectEvent(el))
    el.addEventListener('blur', () => selectEvent(el))
  })
})()
